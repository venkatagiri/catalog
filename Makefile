build:
	cargo build

build-pi: SHELL:=/bin/bash
build-pi:
	if [[ $$(which cross) ]]; then cargo install cross; fi
#	cross build --target arm-unknown-linux-gnueabi

check:
	cargo fmt -- --check
	cargo clippy -- -D warnings

test: check
	cargo test

pre-commit:
	git diff --cached --name-status | grep -q src/ && make test || true

run:
	cargo run ~/.config/catalog/srv

copy-assets:
	command rsync --archive --itemize-changes templates static ~/.config/catalog/

install: build copy-assets
	install target/debug/catalog ~/.cargo/bin
	sudo supervisorctl restart catalog
