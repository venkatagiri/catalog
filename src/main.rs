extern crate catalog;
extern crate structopt;

use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "catalog", about = "browse your hoard")]
struct Opt {
    /// Serve Directory
    serve_dir: PathBuf,

    /// Cache Directory
    #[structopt(short, long, env)]
    cache_dir: Option<PathBuf>,

    /// Asset Directory
    #[structopt(short, long, env)]
    asset_dir: Option<PathBuf>,

    /// URL Base
    #[structopt(short, long, env)]
    url_base: Option<String>,
}

fn main() {
    let opts = Opt::from_args();

    catalog::Config::from_opts(
        opts.serve_dir,
        opts.cache_dir,
        opts.asset_dir,
        opts.url_base,
    )
    .build()
    .launch();
}
