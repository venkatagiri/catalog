use crate::Config;
use rocket::http::Status;
use rocket::local::Client;

#[test]
fn test_root() {
    let rocket = Config::new("/tmp").build();
    let client = Client::new(rocket).unwrap();
    let mut response = client.get("/").dispatch();

    assert!(response.body_string().is_none());
    assert_eq!(response.status(), Status::SeeOther);
    assert_eq!(
        response.headers().get_one("Location"),
        Some("/d/L3RtcA==/tmp")
    );
}

#[test]
fn test_url_base() {
    let custom_url_base = "/custommount";
    let rocket = Config::new("/tmp").url_base(custom_url_base).build();
    let client = Client::new(rocket).unwrap();
    let mut response = client.get("/").dispatch();

    assert!(response.body_string().is_none());
    assert_eq!(response.status(), Status::SeeOther);
    assert_eq!(
        response.headers().get_one("Location"),
        Some(format!("{}/d/L3RtcA==/tmp", custom_url_base).as_str())
    );
}
