#![feature(proc_macro_hygiene, decl_macro)]

extern crate base64;
#[macro_use]
extern crate rocket;
extern crate rocket_contrib;
#[macro_use]
extern crate serde_derive;

mod error;
mod routes;
mod storage;
#[cfg(test)]
mod tests;
mod utils;

use rocket_contrib::serve::StaticFiles;
use rocket_contrib::templates::Template;
use std::env;
use std::path::PathBuf;

/// Builder for our configurations
#[derive(Debug)]
pub struct Config {
    pub serve_dir: PathBuf,
    pub cache_dir: PathBuf,
    pub asset_dir: PathBuf,
    pub url_base: String,
}

impl Config {
    pub fn new(serve_dir: impl Into<PathBuf>) -> Config {
        Config {
            serve_dir: serve_dir.into(),
            cache_dir: default_cache_dir(),
            asset_dir: default_asset_dir(),
            url_base: "".into(),
        }
    }

    pub fn from_opts(
        serve_dir: PathBuf,
        cache_dir: Option<PathBuf>,
        asset_dir: Option<PathBuf>,
        url_base: Option<String>,
    ) -> Config {
        Config {
            serve_dir: serve_dir.canonicalize().unwrap_or(serve_dir),
            cache_dir: cache_dir.unwrap_or_else(default_cache_dir),
            asset_dir: asset_dir.unwrap_or_else(default_asset_dir),
            url_base: url_base.unwrap_or_else(|| "".into()),
        }
    }

    pub fn cache_dir(mut self, cache_dir: impl Into<PathBuf>) -> Config {
        self.cache_dir = cache_dir.into();
        self
    }

    pub fn url_base(mut self, url_base: impl Into<String>) -> Config {
        self.url_base = url_base.into();
        self
    }

    pub fn build(self) -> rocket::Rocket {
        eprintln!("* Config for {}", env!("CARGO_PKG_NAME"));
        eprintln!("    => Serve Dir: {:?}", self.serve_dir);
        eprintln!("    => Cache Dir: {:?}", self.cache_dir);
        eprintln!("    => Asset Dir: {:?}", self.asset_dir);
        eprintln!("    => Url Base:  {:?}", self.url_base);

        env::set_var("ROCKET_TEMPLATE_DIR", self.asset_dir.join("templates"));

        rocket::ignite()
            .mount(
                "/",
                routes![
                    routes::index,
                    routes::directory,
                    routes::file,
                    routes::thumbnail,
                    routes::upload,
                    routes::remove,
                ],
            )
            .mount("/static", StaticFiles::from(self.asset_dir.join("static")))
            .manage(self)
            .attach(Template::fairing())
    }
}

fn default_cache_dir() -> PathBuf {
    let base = env::var("HOME").unwrap_or_else(|_| "/tmp".into());
    PathBuf::from(base)
        .join(".cache")
        .join(env!("CARGO_PKG_NAME"))
}

fn default_asset_dir() -> PathBuf {
    if PathBuf::from("./templates").exists() {
        PathBuf::from(".")
    } else {
        let base = env::var("HOME").unwrap_or_else(|_| "/tmp".into());
        PathBuf::from(base)
            .join(".config")
            .join(env!("CARGO_PKG_NAME"))
    }
}
