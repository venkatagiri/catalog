use std::io;
use std::result;
use std::string;

#[derive(Debug)]
pub enum Error {
    Base64(base64::DecodeError),
    Io(io::Error),
    Utf8Error(string::FromUtf8Error),
    Other(String),
}

impl From<base64::DecodeError> for Error {
    fn from(other: base64::DecodeError) -> Self {
        Error::Base64(other)
    }
}

impl From<io::Error> for Error {
    fn from(other: io::Error) -> Self {
        Error::Io(other)
    }
}

impl From<string::FromUtf8Error> for Error {
    fn from(other: string::FromUtf8Error) -> Self {
        Error::Utf8Error(other)
    }
}

impl From<&str> for Error {
    fn from(other: &str) -> Self {
        Error::Other(other.into())
    }
}

pub type Result<T> = result::Result<T, Error>;
