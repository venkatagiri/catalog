use crate::error::Result;
use crate::storage::File;
use crate::utils::{CachedFile, TemplateContext};
use crate::Config;
use rocket::response::{NamedFile, Redirect};
use rocket::{Data, State};
use rocket_contrib::templates::Template;

#[get("/")]
pub fn index(config: State<Config>) -> Result<Redirect> {
    let file = File::new(config.serve_dir.clone());

    Ok(Redirect::to(format!(
        "{}/d/{}/{}",
        config.url_base, file.slug, file.name
    )))
}

#[get("/d/<slug>/<_name>?<thumbs>")]
pub fn directory(
    slug: String,
    _name: String,
    thumbs: Option<bool>,
    config: State<Config>,
) -> Result<Template> {
    let dir = File::from_slug(slug)?;
    let files = dir.walkit()?;

    Ok(Template::render(
        "directory",
        &TemplateContext {
            title: dir.name.clone(),
            directory: dir.clone(),
            files,
            parent: dir.parent(),
            thumbs: thumbs.unwrap_or(true),
            root: config.serve_dir == dir.path,
        },
    ))
}

#[get("/t/<slug>/<_name>")]
pub fn thumbnail(slug: String, _name: String, config: State<Config>) -> Result<Option<CachedFile>> {
    let file = File::from_slug(slug)?;
    let tpath = file.get_thumbnail(&config.cache_dir)?;

    Ok(NamedFile::open(tpath).ok().map(CachedFile))
}

#[get("/f/<slug>/<_name>")]
pub fn file(slug: String, _name: String) -> Result<Option<CachedFile>> {
    let file = File::from_slug(slug)?;

    Ok(NamedFile::open(&file.path).ok().map(CachedFile))
}

#[post("/u/<slug>/<name>", data = "<data>")]
pub fn upload(slug: String, name: String, data: Data) -> Result<String> {
    let dir = File::from_slug(slug)?;
    let len = data
        .stream_to_file(dir.path.join(&name))
        .map(|n| n.to_string())?;

    Ok(format!(
        "Uploaded File({}) of Size({}) to ({})",
        name, len, dir.name
    ))
}

#[get("/r/<slug>/<_name>")]
pub fn remove(slug: String, _name: String) -> Result<String> {
    let file = File::from_slug(slug)?;
    if file.remove().is_err() {
        Ok(format!("File({}) removal failed!!!", file.name))
    } else {
        Ok(format!("File({}) removed.", file.name))
    }
}
