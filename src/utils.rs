use crate::storage::File;
use rocket::response::{self, NamedFile, Responder};
use rocket::{Request, Response};

/// Allow caching through Cache-Control header
pub struct CachedFile(pub NamedFile);

impl<'r> Responder<'r> for CachedFile {
    fn respond_to(self, req: &Request) -> response::Result<'r> {
        Response::build_from(self.0.respond_to(req)?)
            .raw_header("Cache-control", "max-age=864000")
            .ok()
    }
}

/// Context to hold data and pass it to Handlebars
#[derive(Serialize)]
pub struct TemplateContext {
    pub title: String,
    pub files: Vec<File>,
    pub directory: File,
    pub parent: File,
    pub thumbs: bool,
    pub root: bool,
}
