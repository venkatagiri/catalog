use crate::error::{Error, Result};
use chrono::offset::Local;
use chrono::DateTime;
use std::cmp::Ordering;
use std::ffi::OsStr;
use std::fs;
use std::path::{Path, PathBuf};
use std::process::Command;
use std::time::SystemTime;

#[derive(Serialize, Debug, Clone, Eq)]
pub struct File {
    pub slug: String,
    pub name: String,
    pub path: PathBuf,
    pub is_dir: bool,
    pub size: u64,
    pub modified: String,
}

impl Ord for File {
    fn cmp(&self, other: &Self) -> Ordering {
        self.is_dir
            .cmp(&other.is_dir)
            .reverse()
            .then(self.path.cmp(&other.path))
    }
}

impl PartialOrd for File {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for File {
    fn eq(&self, other: &Self) -> bool {
        self.is_dir == other.is_dir && self.path == other.path
    }
}

impl File {
    pub fn new(path: PathBuf) -> File {
        let path = path.canonicalize().unwrap_or(path);
        let metadata = path.metadata().unwrap(); //FIXME
        let modified: DateTime<Local> = metadata
            .modified()
            .unwrap_or_else(|_| SystemTime::now())
            .into();

        File {
            slug: base64::encode(path.to_string_lossy().into_owned()),
            name: path
                .file_name()
                .unwrap_or_else(|| OsStr::new(""))
                .to_string_lossy()
                .into_owned()
                .replace('[', "(")
                .replace(']', ")"),
            is_dir: metadata.is_dir(),
            size: metadata.len(),
            modified: modified.format("%Y-%m-%d %T").to_string(),
            path,
        }
    }

    pub fn from_slug(slug: String) -> Result<File> {
        let path = base64::decode(slug)?;
        let path = String::from_utf8(path)?;
        let path = PathBuf::from(path);

        Ok(File::new(path))
    }

    pub fn parent(&self) -> File {
        let mut path = self.path.clone();
        path.pop();
        File::new(path)
    }

    pub fn get_thumbnail(&self, cache_dir: &Path) -> Result<PathBuf> {
        let tpath = self.get_thumbnail_path(cache_dir)?;

        if !tpath.exists() {
            let file_type = get_file_type(&self.path)?;

            match file_type.as_ref() {
                "jpeg" | "png" | "gif" => {
                    let fpath = if file_type == "gif" {
                        self.path.with_extension("gif[5]")
                    } else {
                        self.path.clone()
                    };

                    let mut cmd = Command::new("/usr/bin/convert");
                    cmd.args(&["-auto-orient"])
                        .args(&["-quality", "70%"])
                        .args(&["-thumbnail", "x256"])
                        .arg(&fpath)
                        .arg(&tpath);
                    eprintln!("    => Run Cmd: {:?}", cmd);
                    cmd.output()?;
                }
                "webm" | "mp4" | "mkv" => {
                    let mut cmd = Command::new(get_converter());
                    cmd.args(&["-ss", "1"])
                        .args(&[
                            "-i",
                            &self
                                .path
                                .to_str()
                                .ok_or_else(|| Error::from("invalid path!"))?,
                        ])
                        .args(&["-filter:v", "yadif"])
                        .args(&["-f", "mjpeg"])
                        .args(&["-t", "1"])
                        .args(&["-r", "1"])
                        .args(&["-vf", "scale=-1:256"])
                        .arg(&"-an")
                        .arg(&"-y")
                        .arg(&tpath);
                    eprintln!("    => Run Cmd: {:?}", cmd);
                    cmd.output()?;
                }
                "dir" => {
                    if let Some(file) = self
                        .walkit()?
                        .iter()
                        .filter(|f| !f.is_dir)
                        .collect::<Vec<_>>()
                        .first()
                    {
                        let fthumb = file.get_thumbnail(cache_dir)?;
                        fs::copy(fthumb, tpath.clone())?;
                    } else {
                        return Ok(PathBuf::from("./static/thumbnail.png"));
                    }
                }
                _ => return Ok(PathBuf::from("./static/thumbnail.png")),
            }
        }

        Ok(tpath)
    }

    fn get_thumbnail_path(&self, cache_dir: &Path) -> Result<PathBuf> {
        fs::create_dir_all(cache_dir)?;

        let mut file = self.slug.clone();
        file.push_str(".jpg");

        Ok(cache_dir.join(file))
    }

    pub fn remove(&self) -> Result<()> {
        Ok(fs::remove_file(&self.path)?)
    }

    pub fn walkit(&self) -> Result<Vec<File>> {
        let mut files = self
            .path
            .read_dir()?
            .filter_map(|file| file.ok())
            .map(|file| File::new(file.path()))
            .collect::<Vec<File>>();
        files.sort();

        Ok(files)
    }
}

fn get_file_type(fpath: &Path) -> Result<String> {
    let output = Command::new("/usr/bin/file").arg(fpath).output()?;
    let file_type = String::from_utf8(output.stdout)?;

    let ft = if file_type.contains("JPEG image") {
        "jpeg"
    } else if file_type.contains("PNG image") {
        "png"
    } else if file_type.contains("GIF image") {
        "gif"
    } else if file_type.contains("WebM") {
        "webm"
    } else if file_type.contains("MP4 Base Media") || file_type.contains("MP4 v2") {
        "mp4"
    } else if file_type.contains("Matroska") {
        "mkv"
    } else if file_type.contains("directory") {
        "dir"
    } else {
        "unknown"
    };

    Ok(ft.into())
}

fn get_converter() -> String {
    if Path::new("/usr/bin/ffmpeg").exists() {
        "/usr/bin/ffmpeg"
    } else {
        "/usr/bin/avconv"
    }
    .into()
}
